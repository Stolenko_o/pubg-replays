
import Vue from 'vue'
import App from './App'
import BootstrapVue from 'bootstrap-vue'

import router from './router'
import store from './store'

import './styles.js'

Vue.use(BootstrapVue)

window.axios = require('axios')
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

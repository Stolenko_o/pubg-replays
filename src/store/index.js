import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      name: 'Stolen',
      image: '',
      steamid: '',
      email: 'stolenqwerty@gmail.com'
    }
  },
  mutations: {
  }
})

import Vue from 'vue'
import Router from 'vue-router'

import Main from '@/components/Main.vue'
import Replay from '@/components/Replay.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/replay',
      name: 'Replay',
      component: Replay
    },
  ],
  mode: 'history'
})
